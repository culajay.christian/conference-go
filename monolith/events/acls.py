from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(
        f"https://api.pexels.com/v1/search?query={city},{state}", 
        headers=headers
    )
    return (response.json()["photos"][0]["src"]["original"])

def get_weather_data(city, state):
    params = {
        "q": city + "," + state + "," + "US",
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except KeyError:
        return {
            "lat": None,
            "lon": None,
        }
    
    params = {
        "lat": latitude, 
        "lon": longitude, 
        "appid": OPEN_WEATHER_API_KEY
    }

    url = "https://api.openweathermap.org/data/2.5/weather"

    weather_response = requests.get(url, params=params)

    weather_content = json.loads(weather_response.content)


    try:
        weather_dict = {
            "main_temperature": weather_content["main"]["temp"],
            "description": weather_content["weather"][0]["description"]
        }
        return weather_dict
    except KeyError:
        return None